import * as React from 'react';
import logo from './logo.svg';
import './App.less';

export default function App () {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">React Tech Challenge</h1>
      </header>

      <p className="App-intro">
        Enter your postal code to find your MP!
      </p>

      <input />
      <button>Go</button>
    </div>
  );
}
